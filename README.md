# Générateur automatique de musique

##Aperçu
Utiliser python 2.7 et tensorflow pour générer des séquence de musique [Restricted Boltzmann Machine](http://deeplearning4j.org/restrictedboltzmannmachine.html).

##Dépendence

* [Tensorflow](https://www.tensorflow.org/versions/r0.10/get_started/os_setup.html)
* numpy
* msgpack
* glob
* tqdm
* midi
* TKinder
* pygame

Utiliser [pip](https://pypi.python.org/pypi/pip) pour installer les modules (tensorflow, numpy, msgpack-python, glob, tqdm, python-midi)

##Utilisation basique
Pour entrainer l'IA et créer de la musique facilement, simplement lancer la commande suivante.
```
python iaMusic.py
```

L'entrainement va durer entre 5 et 10 sur un ordinateur moderne. Cela va sortir un fichier midi.
