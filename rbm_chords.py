# coding=utf-8
# Ce fichier est hautement basé sur https://github.com/llSourcell/Music_Generator_Demo

# Désactive les informations lié à tensorflow au lancement du programme
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

import tensorflow as tf
import numpy as np
import msgpack
import glob
from tensorflow.python.ops import control_flow_ops
from tqdm import tqdm

###################################################
# Afin que ce projet fonctionn il faut le mettre dans le même dossier
# que le fichier midi_manipulation.py et que le dossier contenant les musiques
import midi_manipulation

class Music_generator:
    song_file = 'Classic'
    x = 0
    W = 0
    bh = 0
    bv = 0

    def __init__(self, name_file):
        Music_generator.song_file = name_file

    def get_songs(self, path):
        # Prise en compte de tous les fichiers .mid* du dossier spécifier
        files = glob.glob('{}/*.mid*'.format(path))
        songs = []
        # tqdm crée une barre de chargement
        for f in tqdm(files):
            try:
                # transforme un fichier midi en une matrice
                song = np.array(midi_manipulation.midiToNoteStateMatrix(f))
                if np.array(song).shape[0] > 50:
                    songs.append(song)
            except Exception as e:
                #raise e
                # En cas d'exception on saut la piste qui pose problème
                continue
        return songs

    # songs = get_songs('Classic') # Toutes les musiques du dossier sont changées en matrice

    #### Fonction utilitaires

    # Génère un sample avec les probabilitiés en paramètre
    def sample(self, probs):
        # Prend un vecteur de probabilités, et retourne un vecteur aléatoire de 0 et 1
        return tf.floor(probs + tf.random_uniform(tf.shape(probs), 0, 1))

    # Lance une chaine de gibbs, que l'on lance deux fois:
    #   - Quand on défini les étapes d'entrainement
    #   - Quand on sample notre music depuis le réseau neuronal
    def gibbs_sample(self, k):
        # Lance un chaine de gibbs sur k étape depuis les probabilités du reseau défini par W, bh et bv
        def gibbs_step(count, k, xk):
            # Lance une étape de gibbs, les valeurs visibles sont initialisées par xk
            hk = self.sample(tf.sigmoid(tf.matmul(xk, self.W) + self.bh)) # Propage les valeur aux couches invisibles
            xk = self.sample(tf.sigmoid(tf.matmul(hk, tf.transpose(self.W)) + self.bv)) # Propage les valeurs aux couches visibles
            return count+1, k, xk

        # Lance une étape de gibbs pour k itération
        ct = tf.constant(0) # compteur
        [_, _, x_sample] = control_flow_ops.while_loop(lambda count, num_iter, *args: count < num_iter,
                                             gibbs_step, [ct, tf.constant(k), self.x])
        return x_sample

    def mainloop(self):
        songs = self.get_songs(self.song_file) # Toutes les musiques du dossier sont changées en matrice
        ###################################################

        ### Parametres

        lowest_note = midi_manipulation.lowerBound # L'index de la note la plus basse jouable sur un clavier
        highest_note = midi_manipulation.upperBound # L'index de la note la plus haute jouable sur un clavier
        note_range = highest_note-lowest_note # La portée des notes

        num_timesteps  = np.random.randint(20, 60) # Le nombre de timesteps créé à la fois
        n_visible      = 2*note_range*num_timesteps # La taille des calques visibles
        n_hidden       = 50 # La taille des calques invisibles

        # Le nombre d'étape d'entrainement que nous allons faire. Pour chaque étape on traverse tout le jeu de données
        num_epochs = 200
        # Le nombre d'exemple d'entrainement que nous allons envoyer dans le réseau Boltzmann restraint à la fois
        batch_size = 100
        # Le taux d'appentissage de notre modèle
        lr         = tf.constant(0.005, tf.float32)

        ### Varibles:

        # La variable placeholder  qui contient nos données
        self.x  = tf.placeholder(tf.float32, [None, n_visible], name="x")
        # La matrice qui enregistre les poids
        self.W  = tf.Variable(tf.random_normal([n_visible, n_hidden], 0.01), name="W")
        # Les biais des couches cachées
        self.bh = tf.Variable(tf.zeros([1, n_hidden],  tf.float32, name="bh"))
        # Les biais des couches visibles
        self.bv = tf.Variable(tf.zeros([1, n_visible],  tf.float32, name="bv"))

        ### Code de mise à jour d'entrainement
        # L'échantillon de x
        x_sample = self.gibbs_sample(1)
        # L'échantillon des noeuds invisibles, démarrant à partir de l'état visible x
        h = self.sample(tf.sigmoid(tf.matmul(self.x, self.W) + self.bh))
        # L'échantillon des noeuds invisibles, démarrant à partir de l'état visible x_sample
        h_sample = self.sample(tf.sigmoid(tf.matmul(x_sample, self.W) + self.bh))

        # On met à jour W, bh et bv, en se basant sur les différence entre les échantillon que nous avons déssiné et les valeurs d'origine
        size_bt = tf.cast(tf.shape(self.x)[0], tf.float32)
        W_adder  = tf.multiply(lr/size_bt, tf.subtract(tf.matmul(tf.transpose(self.x), h), tf.matmul(tf.transpose(x_sample), h_sample)))
        bv_adder = tf.multiply(lr/size_bt, tf.reduce_sum(tf.subtract(self.x, x_sample), 0, True))
        bh_adder = tf.multiply(lr/size_bt, tf.reduce_sum(tf.subtract(h, h_sample), 0, True))
        # Quand on va lancer sess.run(updt), TensorFlow va lancer 3 étapes de mis à jour
        updt = [self.W.assign_add(W_adder), self.bv.assign_add(bv_adder), self.bh.assign_add(bh_adder)]

        ### Lance le graph

        with tf.Session() as sess:
            # On commence par entrainer le model
            # On initialise les variables du model
            init = tf.global_variables_initializer()
            sess.run(init)
            # On parcourt toutes les données d'entrainement num_epochs fois
            for epoch in tqdm(range(num_epochs)):
                for song in songs:
                    # Les sons sont enregistré dans un format temps x note
                    # Ici on redessine les sons pour que chaque exmple d'entrainement soit un vecteur
                    song = np.array(song)
                    song = song[:int(np.floor(song.shape[0]/num_timesteps)*num_timesteps)]
                    song = np.reshape(song, [song.shape[0]/num_timesteps, song.shape[1]*num_timesteps])
                    # Entraine le réseau sur batch_size exemple à la fois
                    for i in range(1, len(song), batch_size):
                        tr_x = song[i:i+batch_size]
                        sess.run(updt, feed_dict={self.x: tr_x})

            # Maintenant le model est pleinement entrainé
            # Lance une chaine de gibbs où les noeuds visibles sont initialisé à 0
            sample = self.gibbs_sample(1).eval(session=sess, feed_dict={self.x: np.zeros((10, n_visible))})
            for i in range(sample.shape[0]):
                if not any(sample[i,:]):
                    continue
                # Ici on redessine le vecteur pour être temps x note, et ensuite on l'enregistre comme fichier midi
                S = np.reshape(sample[i,:], (num_timesteps, 2*note_range))
                midi_manipulation.noteStateMatrixToMidi(S, "generated_chord_{}".format(i))
