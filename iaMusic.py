#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Tkinter import *
import tkFileDialog
import rbm_chords as RBM
import pygame

music_file = ''
def SelectFile():
    global music_file
    music_file = tkFileDialog.askopenfilename(title="Ouvrir le fichier:", filetypes = [("Fichiers MIDI","*.midi"),("Fichiers MID","*.mid")])

def Generer():
	# if (RbMaj.get())==1:
	# 	songs = Music_generator().get_songs('Majeur')
	# elif(RbMin.get())==1:
	# 	songs = Music_generator().get_songs('Mineur')
	# else:
	# 	songs = Music_generator().get_songs('Classic')
    generator = RBM.Music_generator('Classic')
    generator.mainloop()

def Play(music_file):
    pygame.init()
    clock = pygame.time.Clock()
    pygame.mixer.music.load(music_file)
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy():
    # check if playback has finished
        clock.tick(30)

def Stop():
	pygame.mixer.music.stop()


# Création de la fenetre principale
fenetre = Tk()
fenetre.geometry("350x250+250+250")
Label(fenetre, text="IA Musique").pack()
fenetre.title = 'IA Musique'
fenetre['bg']='grey'

frameChoix = Frame(fenetre, borderwidth=2, relief=GROOVE)
frameChoix.pack(side=LEFT, padx=10, pady=10)
frameBtn = Frame(fenetre, borderwidth=2, relief=GROOVE)
frameBtn.pack(side=RIGHT, padx=10, pady=10)

# Radiobuttons choix
Label(frameChoix, text="Mode").pack()
value = StringVar()
RbMaj = Radiobutton(frameChoix, text="Majeur", variable=value, value=1).pack()
RbMin = Radiobutton(frameChoix, text="Mineur", variable=value, value=2).pack()

# Boutons
BtnGenerer = Button(frameBtn, text ='Générer', command=Generer).pack(side=TOP, padx=5, pady=5)
BtnSelectFile = Button(frameBtn, text ='Choix du fichier', command=SelectFile).pack(side=TOP, padx=5, pady=5)
BtnPlay = Button(frameBtn, text ='Play', bg='green', command= lambda: Play(music_file)).pack(side=TOP)
BtnStop = Button(frameBtn, text ="Stop", bg="red").pack(side=TOP)

BtnFermer = Button(fenetre, text="Fermer", command=fenetre.quit).pack(side=BOTTOM, padx=10, pady=10)

fenetre.mainloop()
